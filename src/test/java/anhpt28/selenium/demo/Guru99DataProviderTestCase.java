package anhpt28.selenium.demo;

import static org.testng.Assert.assertEquals;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import anhpt28.selenium.demo.log.LogFactory;
import anhpt28.selenium.demo.model.Guru99BankHomePage;
import anhpt28.selenium.demo.model.Guru99BankManagerHomePage;
import anhpt28.selenium.demo.utils.WebDriverUtils;

public class Guru99DataProviderTestCase {

  private WebDriver driver;
  private WebDriverUtils webDriverUtils;
  private static final Logger LOGGER = LogFactory.getLogger();

  @BeforeClass
  public void setup() throws MalformedURLException {
    System.setProperty("webdriver.chrome.driver", "D:\\FPT\\Course\\Automation\\chromedriver.exe");
    System.setProperty("webdriver.gecko.driver", "D:\\FPT\\Course\\Automation\\geckodriver.exe");
    System.setProperty("webdriver.ie.driver", "D:\\FPT\\Course\\Automation\\IEDriverServer.exe");
    DesiredCapabilities capability = DesiredCapabilities.firefox();
    capability.setBrowserName("firefox");
    capability.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
    capability.setPlatform(Platform.WINDOWS);
    driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);

    // this.webDriverUtils = WebDriverUtils.getInstance();
    // this.driver = this.webDriverUtils.getChromeWebDriver();
  }

  @Test(dataProvider = "Guru99DataProvider")
  public void loginGuru99TestCase(String account, String password) {
    try {
      Guru99BankHomePage guru99BankHomePage = new Guru99BankHomePage(this.driver);
      guru99BankHomePage.goToPage();
      Thread.sleep(1000);
      guru99BankHomePage.login(account, password);
      Thread.sleep(1000);
      Guru99BankManagerHomePage guru99BankManagerHomePage = new Guru99BankManagerHomePage(driver);
      if (guru99BankManagerHomePage.verifyUserId(account)) {
        LOGGER.info("Login success with "+ account +" and "+ password);
        guru99BankManagerHomePage.logout();
        Thread.sleep(1000);
        driver.switchTo().alert().accept();
        Thread.sleep(1000);
      } else {
        LOGGER.info("Login fail with " + account + " and " + password);
        driver.switchTo().alert().accept();
        Thread.sleep(1000);
      }
      assertEquals(true, true);
    } catch (Exception e) {
      LOGGER.error("BUGGGGGGGGGGGGGGGGG:", e);
    }
  }

  @DataProvider(name = "Guru99DataProvider")
  public Object[][] getDataFromDataprovider(Method m) {
    if (m.getName().equalsIgnoreCase("loginGuru99TestCase")) {
      return new Object[][] { { "mngr136913", "bath1234@" }, { "test", "test" }, { "test2", "test2" } };
    }
    return null;
  }

  @AfterClass
  public void close() {
    this.driver.quit();
  }
}