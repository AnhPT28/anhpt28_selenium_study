package anhpt28.selenium.demo.common;

public class WebsiteTestConstants {
	public static final String GOOGLE = "http://www.google.com";
	public static final String FACEBOOK = "https://www.facebook.com";
	public static final String GURU99_UPLOADFILE = "http://demo.guru99.com/test/upload/";
	public static final String GURU99_GENERATE_ACC = "http://demo.guru99.com";
	public static final String GURU99_LOGIN = "http://demo.guru99.com/v4/index.php";
	public static final String TOOL_SQA = "https://www.toolsqa.com/iframe-practice-page/";
	public static final String CEZERIN = "https://store.cezerin.com/";
}
