package anhpt28.selenium.demo.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import anhpt28.selenium.demo.log.LogFactory;
import anhpt28.selenium.demo.model.Guru99Account;

public class DataReaderUtils {
  
  private static DataReaderUtils instance;
  private ExcellReader excellReader;
  private static final Logger LOGGER = LogFactory.getLogger();

  private DataReaderUtils() {
    this.excellReader = ExcellReader.getInstance();
  }

  public static DataReaderUtils getInstance() {
    if (instance == null) {
      synchronized (DataReaderUtils.class) {
        instance = new DataReaderUtils();
      }
    }
    return instance;
  }

  public List<Guru99Account> getGuru99LoginData(String fileName, String sheetName, String title) throws Exception {
    List<Guru99Account> output = new ArrayList<Guru99Account>();
    Sheet sheet = this.excellReader.getExcellSheet(fileName, sheetName);
    for (int i = 0; i < 3; i++) {
      Row row = sheet.getRow(i + 1);
      Cell userCell = row.getCell(1);
      Cell passwordCell = row.getCell(2);
      LOGGER.info(userCell.getStringCellValue());
      LOGGER.info(passwordCell.getStringCellValue());
      Guru99Account account = new Guru99Account();
      account.setUser(userCell.getStringCellValue());
      account.setPassword(passwordCell.getStringCellValue());
      output.add(account);
    }
    return output;
  }
}
