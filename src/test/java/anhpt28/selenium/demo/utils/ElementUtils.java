package anhpt28.selenium.demo.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ElementUtils {

	private static ElementUtils instance;

	private ElementUtils() {
	}

	public static ElementUtils getInstance() {
		if (instance == null) {
			synchronized (ElementUtils.class) {
				instance = new ElementUtils();
			}
		}
		return instance;
	}

	public boolean isPresent(WebDriver driver, WebElement element, int timeOut) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, timeOut);
			wait.until(ExpectedConditions.visibilityOf(element));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
