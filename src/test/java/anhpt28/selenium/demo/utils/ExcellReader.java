package anhpt28.selenium.demo.utils;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcellReader {

  private static ExcellReader instance;

  private ExcellReader() {
  }

  public static ExcellReader getInstance() {
    if (instance == null) {
      synchronized (ExcellReader.class) {
        instance = new ExcellReader();
      }
    }
    return instance;
  }

  public Sheet getExcellSheet(String fileName, String sheetName) throws Exception {
    FileInputStream inputStream = new FileInputStream(
        new File(getClass().getClassLoader().getResource(fileName).getFile()));
    String fileExtensionName = fileName.substring(fileName.indexOf("."));
    Workbook workbook = null;
    if (fileExtensionName.equals(".xlsx")) {
      workbook = new XSSFWorkbook(inputStream);
    } else if (fileExtensionName.equals(".xls")) {
      workbook = new HSSFWorkbook(inputStream);
    }
    Sheet sheet = workbook.getSheet(sheetName);
    return sheet;
  }
}
