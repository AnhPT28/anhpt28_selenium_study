package anhpt28.selenium.demo.model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Guru99BankManagerHomePage {

  private static final String WEB_URL = "http://demo.guru99.com/v4/manager/Managerhomepage.php";
  private static final String USERID_OUTPUT_TEXT = "//tr[@class=\"heading3\"]/td";
  private static final String AGILE_PROJECT_MENU_XPATH = "//a[contains(.,'Agile Project')]";
  private static final String LOGOUT_MENU_XPATH = "//a[contains(.,'Log out')]";
  private static final String MARQUEE_XPATH = "//marquee";
  private WebDriver driver;

  public Guru99BankManagerHomePage(WebDriver driver) {
    this.driver = driver;
  }
  
  public boolean verifyUserId(String expected) {
    try {
      WebElement marquee = this.driver.findElement(By.xpath(MARQUEE_XPATH));
      return true;
    } catch (Exception e) {
      return false;
    }
    
  }
  
  public void clickAgileProjectMenu() {
    WebElement agileProjectMenu = this.driver.findElement(By.xpath(AGILE_PROJECT_MENU_XPATH));
    agileProjectMenu.click();
  }
  
  public void logout() {
    WebElement agileProjectMenu = this.driver.findElement(By.xpath(LOGOUT_MENU_XPATH));
    agileProjectMenu.click();
  }
  
}
