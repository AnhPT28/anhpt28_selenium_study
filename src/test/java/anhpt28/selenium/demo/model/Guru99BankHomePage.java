package anhpt28.selenium.demo.model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Guru99BankHomePage {
  
  private static final String WEB_URL = "http://demo.guru99.com/v4/index.php";
  private static final String USERID_INPUT_XPATH = "//input[@name=\"uid\"]";
  private static final String PASSWORD_INPUT_XPATH = "//input[@name=\"password\"]";
  private static final String BUTTON_LOGIN_XPATH = "//input[@name=\"btnLogin\"]";
  private WebDriver driver;

  public Guru99BankHomePage(WebDriver driver) {
    this.driver = driver;
  }
  
  public void goToPage() {
    this.driver.get(WEB_URL);
  }
  
  public void login(String userId, String password) {
    WebElement userIdInputField = this.driver.findElement(By.xpath(USERID_INPUT_XPATH));
    userIdInputField.sendKeys(userId);
    
    WebElement passwordInputField = this.driver.findElement(By.xpath(PASSWORD_INPUT_XPATH));
    passwordInputField.sendKeys(password);
    
    WebElement loginButton = this.driver.findElement(By.xpath(BUTTON_LOGIN_XPATH));
    loginButton.click();
  }
  
}
