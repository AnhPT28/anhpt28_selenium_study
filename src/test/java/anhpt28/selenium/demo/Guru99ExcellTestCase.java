//package anhpt28.selenium.demo;
//
//import java.util.List;
//
//import org.apache.log4j.Logger;
//import org.openqa.selenium.WebDriver;
//import org.testng.annotations.AfterClass;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//import anhpt28.selenium.demo.log.LogFactory;
//import anhpt28.selenium.demo.model.Guru99Account;
//import anhpt28.selenium.demo.model.Guru99BankHomePage;
//import anhpt28.selenium.demo.model.Guru99BankManagerHomePage;
//import anhpt28.selenium.demo.utils.DataReaderUtils;
//import anhpt28.selenium.demo.utils.WebDriverUtils;
//
//public class Guru99ExcellTestCase {
//
//  private static final String FILE_DATA = "Guru99LoginTestData.xlsx";
//  private static final String LOGIN_SHEET_NAME = "Sheet1";
//  private static final String LOGIN_TITLE = "Guru99LoginTestCase";
//  private WebDriver driver;
//  private WebDriverUtils webDriverUtils;
//  private static final Logger LOGGER = LogFactory.getLogger();
//
//  @BeforeClass
//  public void setup() {
//    this.webDriverUtils = WebDriverUtils.getInstance();
//    this.driver = this.webDriverUtils.getChromeWebDriver();
//  }
//  
//  @Test
//  public void loginGuru99TestCase() throws Exception {
//    Guru99BankHomePage guru99BankHomePage = new Guru99BankHomePage(this.driver);
//    guru99BankHomePage.goToPage();
//    Thread.sleep(1000);
//    List<Guru99Account> listAccount = DataReaderUtils.getInstance().getGuru99LoginData(FILE_DATA, LOGIN_SHEET_NAME, LOGIN_TITLE);
//    for (Guru99Account guru99Account : listAccount) {
//      guru99BankHomePage.login(guru99Account.getUser(), guru99Account.getPassword());
//      Thread.sleep(1000);
//      Guru99BankManagerHomePage guru99BankManagerHomePage = new Guru99BankManagerHomePage(driver);
//      if (guru99BankManagerHomePage.verifyUserId(guru99Account.getUser())){
//        LOGGER.info("Login success with "+ guru99Account.getUser() +" and "+ guru99Account.getPassword());
//        guru99BankManagerHomePage.logout();
//        Thread.sleep(1000);
//        driver.switchTo().alert().accept();
//        Thread.sleep(1000);
//      } else {
//        LOGGER.info("Login fail with "+ guru99Account.getUser() +" and "+ guru99Account.getPassword());
//        driver.switchTo().alert().accept();
//        Thread.sleep(1000);
//      }
//    }
//  }
//  
//  @AfterClass
//  public void close() {
//    this.driver.close();
//    this.driver.quit();
//  }
//}
