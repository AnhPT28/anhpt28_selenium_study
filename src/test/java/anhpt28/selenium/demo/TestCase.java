//package anhpt28.selenium.demo;
//
//import java.util.Date;
//import java.util.Iterator;
//
//import org.apache.log4j.Logger;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.support.ui.Select;
//import org.testng.Assert;
//import org.testng.annotations.AfterClass;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//import anhpt28.selenium.demo.common.InputConstants;
//import anhpt28.selenium.demo.common.WebsiteTestConstants;
//import anhpt28.selenium.demo.common.XpathConstants;
//import anhpt28.selenium.demo.log.LogFactory;
//import anhpt28.selenium.demo.utils.WebDriverUtils;
//
//public class TestCase {
//
//	private WebDriver driver;
//	private WebDriverUtils webDriverUtils;
//	private static final Logger LOGGER = LogFactory.getLogger();
//
//	@BeforeClass
//	public void setup() {
//		this.webDriverUtils = WebDriverUtils.getInstance();
//		driver = this.webDriverUtils.getChromeWebDriver();
//	}
//
//	@Test(invocationCount = 5)
//	public void openGoogle() {
//		driver.get(WebsiteTestConstants.GOOGLE);
//		LOGGER.info("Page Title is " + driver.getTitle());
//		Assert.assertEquals("Google", driver.getTitle());
//	}
//
//	@Test
//	public void loginFacebook() throws InterruptedException {
//		driver.get(WebsiteTestConstants.FACEBOOK);
//		LOGGER.info("Successfully opened Facebook");
//		driver.manage().window().maximize();
//		driver.findElement(By.xpath(XpathConstants.FACEBOOK_TXT_EMAIL)).sendKeys(InputConstants.FACEBOOK_USERNAME);
//		driver.findElement(By.xpath(XpathConstants.FACEBOOK_TXT_PASSWORD)).sendKeys(InputConstants.FACEBOOK_PASSWORD);
//		driver.findElement(By.xpath(XpathConstants.FACEBOOK_BUTTON_LOGIN)).click();
//		LOGGER.info("Successfully logged in Facebook");
//		Thread.sleep(3000);
//		driver.findElement(By.xpath(XpathConstants.FACEBOOK_BUTTON_USERNAV)).click();
//		Thread.sleep(3000);
//		driver.findElement(By.xpath(XpathConstants.FACEBOOK_BUTTON_LOGOUT)).click();
////		driver.find_element_by_xpath("//span[text()='Log Out']").click();
////		driver.findElement(By.partialLinkText("Log Out")).click();
////		driver.findElement(By.xpath("//span[@class='_54nh'][contains(.,'Đăng xuất')]")).click();
//		Thread.sleep(3000);
//		LOGGER.info("Successfully logged out from Facebook");
//		Assert.assertEquals(true, true);
//	}
//
//	@Test
//	public void uploadFileGuru99() throws InterruptedException {
//		driver.get(WebsiteTestConstants.GURU99_UPLOADFILE);
//		LOGGER.info("Successfully opened GURU99 upload file");
//		driver.manage().window().maximize();
//		driver.findElement(By.xpath(XpathConstants.GURU99_TXT_INPUTFILE)).sendKeys(InputConstants.GURU99_FILEINPUT);
//		driver.findElement(By.xpath(XpathConstants.GURU99_CHECKBOX_TERMS)).click();
//		driver.findElement(By.xpath(XpathConstants.GURU99_BUTTON_SUBMIT)).click();
//		Thread.sleep(2000);
//		LOGGER.info("Submit ok");
//		Assert.assertEquals(true, true);
//	}
//
//	@Test
//	public void generateAccAndLoginGuru99() throws InterruptedException {
//		driver.get(WebsiteTestConstants.GURU99_GENERATE_ACC);
//		LOGGER.info("Successfully opened GURU99 Generate acc");
//		driver.manage().window().maximize();
//		driver.findElement(By.xpath(XpathConstants.GURU99_GENERATE_ACC_TXT_EMAIL))
//				.sendKeys(InputConstants.GURU99_EMAIL_REGISTER);
//		driver.findElement(By.xpath(XpathConstants.GURU99_GENERATE_ACC_BUTTON_SUBMIT)).click();
//		Thread.sleep(2000);
//
//		String userID = driver.findElement(By.xpath(XpathConstants.GURU99_HOME_TXT_USER)).getText().trim();
//		String password = driver.findElement(By.xpath(XpathConstants.GURU99_HOME_TXT_PASSWORD)).getText().trim();
//
//		driver.navigate().to(WebsiteTestConstants.GURU99_LOGIN);
//		driver.findElement(By.xpath(XpathConstants.GURU99_LOGIN_TXT_USER)).sendKeys(userID);
//		driver.findElement(By.xpath(XpathConstants.GURU99_LOGIN_TXT_PASSWORD)).sendKeys(password);
//		driver.findElement(By.xpath(XpathConstants.GURU99_LOGIN_BUTTON_SUBMIT)).click();
//		Thread.sleep(2000);
//
//		String userIdPresent = driver.findElement(By.xpath(XpathConstants.GURU99_MANAGER_TXT_USERID)).getText();
//		if (userIdPresent.contains(userID)) {
//			LOGGER.info("Login GURU99 success");
//		}
//
//		driver.findElement(By.xpath(XpathConstants.GURU99_MANAGER_NAV_AGILE)).click();
//		Thread.sleep(2000);
//		String userAccess = driver.findElement(By.xpath(XpathConstants.GURU99_AGILE_TXT_USER_ACCESS)).getText()
//				.replace("UserID : ", "");
//		String passwordAccess = driver.findElement(By.xpath(XpathConstants.GURU99_AGILE_TXT_PASSWORD_ACCESS)).getText()
//				.replace("Password : ", "");
//		;
//
//		driver.findElement(By.xpath(XpathConstants.GURU99_AGILE_TXT_USER)).sendKeys(userAccess);
//		driver.findElement(By.xpath(XpathConstants.GURU99_AGILE_TXT_PASSWORD)).sendKeys(passwordAccess);
//		driver.findElement(By.xpath(XpathConstants.GURU99_AGILE_BUTTON_SUBMIT)).click();
//		Thread.sleep(2000);
//
//		String welcome = driver.findElement(By.xpath(XpathConstants.GURU99_AGILE_TXT_LOGIN_WELCOME)).getText();
//		if (welcome.equals("Welcome To Customer's Page of Guru99 Bank")) {
//			LOGGER.info("Login GURU99 agile success");
//		}
//
//		Assert.assertEquals(true, true);
//	}
//
//	@Test
//	public void iframeToolSQA() throws InterruptedException {
//		driver.get(WebsiteTestConstants.TOOL_SQA);
//		LOGGER.info("Successfully opened Tool Sqa");
//		driver.manage().window().maximize();
//		driver.switchTo().frame("iframe1");
//		Thread.sleep(1000);
//
//		driver.findElement(By.xpath(XpathConstants.TOOLSQA_FRAME1_TXT_FIRSTNAME))
//				.sendKeys(InputConstants.TOOLSQA_FRAME1_TXT_FIRSTNAME);
//		driver.findElement(By.xpath(XpathConstants.TOOLSQA_FRAME1_TXT_LASTNAME))
//				.sendKeys(InputConstants.TOOLSQA_FRAME1_TXT_LASTNAME);
//		driver.findElement(By.xpath(XpathConstants.TOOLSQA_FRAME1_RADIO_SEX_MALE)).click();
//		driver.findElement(By.xpath(XpathConstants.TOOLSQA_FRAME1_RADIO_1_YEAR_EXP)).click();
//		driver.findElement(By.xpath(XpathConstants.TOOLSQA_FRAME1_TXT_DATE)).sendKeys(new Date().toString());
//		driver.findElement(By.xpath(XpathConstants.TOOLSQA_FRAME1_RADIO_PRO)).click();
//
//		LOGGER.info("Done input");
//
//		Assert.assertEquals(true, true);
//	}
//
//	@Test
//	public void loginAndTweetInTwitter() throws InterruptedException {
//		driver.get(WebsiteTestConstants.TOOL_SQA);
//		LOGGER.info("Successfully opened Tool Sqa");
//		driver.manage().window().maximize();
//		driver.findElement(By.xpath(XpathConstants.TOOLSQA_BUTTON_SHARE_TWEET)).click();
//		Thread.sleep(2000);
//		LOGGER.info("Successfully opened share Tweet");
//
//		Iterator<String> windows = driver.getWindowHandles().iterator();
//		while (windows.hasNext()) {
//			driver.switchTo().window(windows.next());
//			if (driver.getTitle().equalsIgnoreCase("Share a link on Twitter")) {
//				break;
//			}
//		}
//		driver.findElement(By.xpath(XpathConstants.TWITTER_TXT_ACC)).sendKeys(InputConstants.TWITTER_ACC);
//		driver.findElement(By.xpath(XpathConstants.TWITTER_TXT_PASSWORD)).sendKeys(InputConstants.TWITTER_PASSWORD);
//		driver.findElement(By.xpath(XpathConstants.TWITTER_BUTTON_LOGIN_AND_TWEET)).click();
//		LOGGER.info("Successfully Tweet");
//		Assert.assertEquals(true, true);
//	}
//	
//	@Test
//	public void cezerinTest() throws InterruptedException {
//		driver.get(WebsiteTestConstants.CEZERIN);
//		LOGGER.info("Successfully opened the website");
//		Actions builder = new Actions(driver);
//		builder.moveToElement(driver.findElement(By.xpath(XpathConstants.CEZERIN_NAV_FOOTWEAR))).perform();
//		builder.moveToElement(driver.findElement(By.xpath(XpathConstants.CEZERIN_NAV_FOOTWEAR_TRAINERS))).click().perform();
//		Thread.sleep(2000);
//		driver.findElement((By.xpath(XpathConstants.CEZERIN_SIZE_11))).click();
//		Thread.sleep(2000);
//		Select sort = new Select(driver.findElement(By.xpath(XpathConstants.CEREZIN_SELECT_SORT)));
//		sort.selectByVisibleText("Price low to high");
//		Thread.sleep(2000);
//		driver.findElement(By.xpath(XpathConstants.CEREZIN_SORT_FIRST_PRODUCT)).click();
//		Thread.sleep(2000);
//		driver.findElement(By.xpath(XpathConstants.CEREZIN_PRODUCT_QUANTITY)).sendKeys("3");
//		driver.findElement(By.xpath(XpathConstants.CEREZIN_BUTTON_ADD_TO_CART)).click();
//		Thread.sleep(1000);
//		driver.findElement(By.xpath(XpathConstants.CEREZIN_BUTTON_GO_TO_CHECKOUT)).click();
//		Thread.sleep(1000);
//		Select quantityCheckout = new Select(driver.findElement(By.xpath(XpathConstants.CEREZIN_SELECT_QUANTITY)));
//		quantityCheckout.selectByVisibleText("1");
//		
//		// typing email
//		WebElement email = driver.findElement(By.xpath("//*[@id=\"customer.email\"]"));
//		email.clear();
//		email.sendKeys(InputConstants.CEZERIN_EMAIL);
//		
//		// typing mobile
//		WebElement mobile = driver.findElement(By.xpath("//*[@id=\"customer.mobile\"]"));
//		mobile.clear();
//		mobile.sendKeys(InputConstants.CEZERIN_MOBILE);
//    
//		// typing country
//    WebElement country = driver.findElement(By.xpath("//*[@id=\"shipping_address.country\"]"));
//    country.clear();
//    country.sendKeys(InputConstants.CEZERIN_COUNTRY);
//		
//		// typing state
//		WebElement state = driver.findElement(By.xpath("//*[@id=\"shipping_address.state\"]"));
//		state.clear();
//		state.sendKeys(InputConstants.CEZERIN_STATE);
//		
//		// typing city
//		WebElement city = driver.findElement(By.xpath("//*[@id=\"shipping_address.city\"]"));
//		city.clear();
//		city.sendKeys(InputConstants.CEZERIN_CITY);
//		
//		// check shipping option: free shipping
//		driver.findElement(By.xpath("//*[@id=\"app\"]/section[1]/div/div/div[2]/div/div[1]/form/div[6]/label[1]/input")).click();
//		Thread.sleep(500);
//		
//		// check payment option: cash
//		driver.findElement(By.xpath("//*[@id=\"app\"]/section[1]/div/div/div[2]/div/div[1]/form/div[7]/label/div/div[1]")).click();
//		
//		// click next
//		driver.findElement(By.xpath("//*[@id=\"app\"]/section[1]/div/div/div[2]/div/div[1]/form/div[8]/button")).click();
//
//		Thread.sleep(500);
//		String emailShow = driver.findElement(By.xpath("//*[@id=\"app\"]/section[1]/div/div/div[2]/div/div[1]/div[1]/div[2]")).getText();
//		String mobileShow = driver.findElement(By.xpath("//*[@id=\"app\"]/section[1]/div/div/div[2]/div/div[1]/div[2]/div[2]")).getText();
//		String countryShow = driver.findElement(By.xpath("//*[@id=\"app\"]/section[1]/div/div/div[2]/div/div[1]/div[3]/div[2]")).getText();
//		String stateShow = driver.findElement(By.xpath("//*[@id=\"app\"]/section[1]/div/div/div[2]/div/div[1]/div[4]/div[2]")).getText();
//		String cityShow = driver.findElement(By.xpath("//*[@id=\"app\"]/section[1]/div/div/div[2]/div/div[1]/div[5]/div[2]")).getText();
//		
//		Assert.assertEquals(emailShow, InputConstants.CEZERIN_EMAIL);
//		Assert.assertEquals(mobileShow, InputConstants.CEZERIN_MOBILE);
//		Assert.assertEquals(countryShow, InputConstants.CEZERIN_COUNTRY);
//		Assert.assertEquals(stateShow, InputConstants.CEZERIN_STATE);
//		Assert.assertEquals(cityShow, InputConstants.CEZERIN_CITY);
//		
//	}
//
//	@Test
//	public void example() throws InterruptedException {
//		driver.get(WebsiteTestConstants.GURU99_LOGIN);
//		LOGGER.info("Successfully opened the website");
//
//		WebElement userID = driver.findElement(By.name("uid"));
//		if (userID.isDisplayed()) {
//			LOGGER.info("Get User ID textfield by name");
//		}
//		userID = driver.findElement(By.cssSelector("input[type=\"text\"]"));
//		if (userID.isDisplayed()) {
//			LOGGER.info("Get User ID textfield by css");
//		}
//		userID = driver.findElement(By.xpath("/html/body/form/table/tbody/tr[1]/td[2]/input"));
//		if (userID.isDisplayed()) {
//			LOGGER.info("Get User ID textfield by xpath");
//		}
//
//		WebElement password = driver.findElement(By.name("password"));
//		if (password.isDisplayed()) {
//			LOGGER.info("Get password textfield by name");
//		}
//		password = driver.findElement(By.cssSelector("input[type=\"password\"]"));
//		if (password.isDisplayed()) {
//			LOGGER.info("Get password textfield by css");
//		}
//		password = driver.findElement(By.xpath("/html/body/form/table/tbody/tr[2]/td[2]/input"));
//		if (password.isDisplayed()) {
//			LOGGER.info("Get password textfield by xpath");
//		}
//
//		WebElement loginButton = driver.findElement(By.name("btnLogin"));
//		if (loginButton.isDisplayed()) {
//			LOGGER.info("Get loginButton by name");
//		}
//		loginButton = driver.findElement(By.cssSelector("input[type=\"submit\"]"));
//		if (loginButton.isDisplayed()) {
//			LOGGER.info("Get loginButton by css");
//		}
//		loginButton = driver.findElement(By.xpath("/html/body/form/table/tbody/tr[3]/td[2]/input[1]"));
//		if (loginButton.isDisplayed()) {
//			LOGGER.info("Get loginButton by xpath");
//		}
//
//		WebElement resetButton = driver.findElement(By.name("btnReset"));
//		if (resetButton.isDisplayed()) {
//			LOGGER.info("Get loginButton by name");
//		}
//		resetButton = driver.findElement(By.cssSelector("input[type=\"reset\"]"));
//		if (resetButton.isDisplayed()) {
//			LOGGER.info("Get loginButton by css");
//		}
//		resetButton = driver.findElement(By.xpath("/html/body/form/table/tbody/tr[3]/td[2]/input[2]"));
//		if (resetButton.isDisplayed()) {
//			LOGGER.info("Get loginButton by xpath");
//		}
//
//		Assert.assertEquals(true, true);
//	}
//	
//	@Test
//	public void iframeToolSQA() throws InterruptedException {
//		driver.navigate().to("https://www.tienganh123.com/dich-cau-viet-anh");
//		LOGGER.info("Successfully opened Tool Sqa");
//		driver.manage().window().maximize();
//		Thread.sleep(1000);
//
//
//		LOGGER.info("Done input");
//
//		Assert.assertEquals(true, true);
//	}
//
//	@AfterClass
//	public void close() {
//		driver.close();
//		driver.quit();
//	}
//
//}